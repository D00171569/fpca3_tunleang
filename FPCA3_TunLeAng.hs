import System.Random
--a function which take in 3 double and calculate the quadratic equation
solve_quadratic_equation::Double->Double->Double->(Double,Double)
solve_quadratic_equation a b c
   |d>=0=(x1,x2)
   |d<0=error "dont know how to find complex roots"
   where        
      x1 = (-b + sd)/(2*a)
      x2 = (-b - sd)/(2*a)
      d = b**2 -4*a*c
      sd = sqrt d
--return a string to tell user that his/her answer is correct or not
checkEquationAnswer::(Double,Double)->Double->Double->String
checkEquationAnswer (a,b) x y =
   if a==x && b==y
      then "Correct!"
      else "Wrong!"
checkAnswer :: (Eq a,Num a) => a -> a -> String  
checkAnswer a b  
    | a == b = "Correct!"      
    | otherwise = "Wrong!"
--to get a Operator and print it out
getOperator :: (Eq a,Num a) => a -> String  
getOperator a
   | a == 1 = "+"
   | a == 2 = "-"
   | a == 3 = "/"
   | a == 4 = "*"
   | otherwise = "*"
--to do the calculation to get the correct answer
calculate::(Integral a,Eq a)=>a->a->a->a
calculate x y z
   |x == 1 = y + z
   |x == 2 = y - z
   |x == 3 = y `div` z
   |x == 4 = y * z
   |otherwise =y+z
--allow user to choose level and set the range of the random number
selectLevel::(Eq a,Integral a)=>a->a
selectLevel a 
   |a == 1 = 10
   |a == 2 = 99
   |a == 3 = 999
   |otherwise = 10
--to check the result from user if yes return true else false
playAgain::String->Bool
playAgain a
   |a=="Y"=True
   |a=="y"=True
   |otherwise = False

printAward::Int->String
printAward a
   |a>=5="Well Done.Keep Going"
   |a>=10="Really Good Job"
   |a>=20="Awesome"
   |otherwise="Not Far From the AWARD Keep Going"
countScore::Int->Int
countScore a = a+1



main = do   
   putStrLn "Select Question Type: 1)Normal,2)Quadratic Equation"
   input<-getLine
   if input == "1"
      then question
      else equation
--when the main called question, will run the code below
question = do
   putStrLn "Math quiz"  
--able the user to select which level to play   
   putStrLn "Select Level: 1)Easy,2)Normal,3)Hard"
   input<-getLine
   let selectedLevel = read input ::Int
   let level = selectLevel selectedLevel
   --generate an infinite list of random number
   gen <- newStdGen
   let ns = randomRs (1,level) gen :: [Int]
   --take 2 element from the infinite list
   let xs = take 2 ns
   --take first number from the list
   let n1 = head xs
   --take second number from the list, have use head one more time
   --since the tail will return a list
   let n2 = head $ tail xs   
   putStrLn "Select an Operator:1)Addition,2)Subtraction,3)Division,4)Multiplication"
   userInput<-getLine
   let userOperator=read userInput::Int
   let operator = getOperator userOperator
   --call the function calculate to calculate the correct answer
   let answer = calculate userOperator n1 n2
   print n1 
   print operator
   print n2
   --get the answer from user and convert it into a Int
   putStrLn "Enter your answer:"
   userAnswer<-getLine
   putStrLn $"Your answer is " ++userAnswer
   let number = read userAnswer :: Int
   --to print correct if answer is correct else print wrong
   print $ checkAnswer number answer
   print $ "the Correct ANSWER is"
   print answer   
   let scores = 0
   let award =printAward scores
   if(answer == number)
      then print $countScore scores
      else print scores
   --print scores
   print award
   putStrLn "Play Again: (Y/N)"
   userAgain<-getLine
   let result= playAgain userAgain
   --after got the result from the user if user parse Y/y
   --it will call main again and allow user to choose the question type again
   --else will print "Bye"
   if result
      then main
      else putStrLn "Bye"

--when main call equation, will run the code below:
equation = do
   gen <- newStdGen
   let ns = randomRs (1,10) gen :: [Double] 
   let xs = take 3 ns
   --to get the frist number of xs
   let n1 = head xs
   --to get the second number of xs
   let n2 = head $ tail xs
   --to get the thrid number of xs
   let n3 = head $ tail $ tail xs 
   let answer = solve_quadratic_equation n1 n2 n3
   print n1
   print "x^2"
   print n2
   print "x"
   print n3
   --to get the answer from user and convert the answer into a double
   putStrLn "Enter First number of your answer:"
   userAnswer<-getLine
   putStrLn $"Your First number of your answer is " ++userAnswer
   let number1 = read userAnswer :: Double
   --
   putStrLn "Enter Second number of your answer:"
   userAnswer2<-getLine
   putStrLn $"Your Second number of your answer is " ++userAnswer2
   let number2 = read userAnswer2 :: Double
   --
   print $ checkEquationAnswer answer number1 number2
   print answer
   putStrLn "Play Again: (Y/N)"
   userAgain<-getLine
   let result= playAgain userAgain
   if result
      then main
      else putStrLn "Bye"

   
   
--trying to make the random quadratic equation pick from a list
   